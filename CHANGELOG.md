All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog]
and this project adheres to [Semantic Versioning].

[Keep a Changelog]: https://keepachangelog.com/en/1.0.0/
[Semantic Versioning]: https://semver.org/spec/v2.0.0.html

[Unreleased]
============

N/A

[0.0.3] - 2018-04-27
====================

Added
-----

  - Started keeping a change log.

Changed
-------

  - Fixed compatibility with `attrs` >= 17.3.0

[0.0.2] - 2018-04-25
====================

Added
-----

  - `rustbud` now maintains a shared `credentials` file for cargo's interactions
    with <https://crates.io/>, and symlinks it into place in each environment.
    
[0.0.1] - 2017-09-26
====================

Initial release.

[Unreleased]: https://gitlab.com/Screwtapello/rustbud/compare/v0.0.3...master
[0.0.3]: https://gitlab.com/Screwtapello/rustbud/compare/v0.0.2...v0.0.3
[0.0.2]: https://gitlab.com/Screwtapello/rustbud/compare/2c248274e8...v0.0.2
[0.0.1]: https://gitlab.com/Screwtapello/rustbud/commit/2c248274e8a83ee5d8295cbe36eb199f57d28bba
