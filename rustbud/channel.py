import collections
import datetime
import io

import attr


@attr.s
class Channel:

    date = attr.ib(
        validator=attr.validators.instance_of(datetime.date),
    )
    packages = attr.ib(
        validator=attr.validators.instance_of(dict),
    )

    @classmethod
    def from_manifest(cls, manifest):
        if manifest["manifest-version"] != "2":
            raise ValueError(
                "Unknown manifest version {!r}".format(
                    manifest["manifest-version"],
                )
            )

        date = datetime.datetime.strptime(
            manifest["date"],
            "%Y-%m-%d",
        ).date()

        packages = {
            name: Package.from_manifest(contents)
            for name, contents in manifest["pkg"].items()
        }

        return cls(date, packages)


@attr.s
class Package:

    targets = attr.ib(
        validator=attr.validators.instance_of(dict),
    )
    version = attr.ib(
        validator=attr.validators.instance_of(str),
    )

    @classmethod
    def from_manifest(cls, manifest):
        targets = {
            triple: Component.from_manifest(content)
            for triple, content in manifest["target"].items()
        }
        version = manifest["version"]

        return cls(targets, version)

    def component_for_target(self, target):
        # If we have the requested target, provide it.
        if target in self.targets:
            return self.targets[target]

        # Targets that are not specific to a target (like docs)
        # are given the special target "*".
        if "*" in self.targets:
            return self.targets["*"]

        raise KeyError(target)


@attr.s
class Component:

    artifacts = attr.ib(
        validator=attr.validators.instance_of(dict),
    )
    required_components = attr.ib(
        validator=attr.validators.instance_of(list),
    )
    optional_components = attr.ib(
        validator=attr.validators.instance_of(list),
    )

    @classmethod
    def from_manifest(cls, manifest):
        artifacts = {}
        if manifest["available"]:
            if "hash" in manifest:
                artifacts["gz"] = ArtifactRef(
                    hash=manifest["hash"],
                    url=manifest["url"],
                )
            if "xz_hash" in manifest:
                artifacts["xz"] = ArtifactRef(
                    hash=manifest["xz_hash"],
                    url=manifest["xz_url"],
                )
        required_components = [
            ComponentRef.from_manifest(content)
            for content in manifest.get("components", [])
        ]
        optional_components = [
            ComponentRef.from_manifest(content)
            for content in manifest.get("extensions", [])
        ]

        return cls(
            artifacts,
            required_components,
            optional_components,
        )

    def dereference(self, channel):
        return self

    def choose_artifact(self, preferences):
        for each in preferences:
            if each in self.artifacts:
                return self.artifacts[each]

        raise LookupError(preferences)

    @property
    def all_components(self):
        res = []
        res.extend(self.required_components)
        res.extend(self.optional_components)
        return res


@attr.s(frozen=True)
class ArtifactRef:

    hash = attr.ib(
        validator=attr.validators.instance_of(str),
        cmp=True,
    )
    url = attr.ib(
        validator=attr.validators.instance_of(str),
        cmp=True,
    )

    @classmethod
    def from_data(cls, data):
        return cls(
            hash=data["hash"],
            url=data["url"],
        )

    def to_data(self):
        return {
            "hash": self.hash,
            "url": self.url,
        }


@attr.s(frozen=True)
class ComponentRef:

    package = attr.ib(
        validator=attr.validators.instance_of(str),
        cmp=True,
    )
    target = attr.ib(
        validator=attr.validators.instance_of(str),
        cmp=True,
    )

    @classmethod
    def from_manifest(cls, manifest):
        package = manifest["pkg"]
        target = manifest["target"]

        return cls(package, target)

    def dereference(self, channel):
        package = channel.packages[self.package]
        component = package.component_for_target(self.target)
        return component


def gather_targets_by_package(refs):
    res = collections.defaultdict(set)

    for each in refs:
        res[each.package].add((each.target, each))

    return res


def format_refs(refs, stream, channel):
    targets_by_package = gather_targets_by_package(refs)
    for package in sorted(targets_by_package.keys()):
        print("  {}:".format(package), file=stream)
        for target, ref in sorted(targets_by_package[package]):
            try:
                component = ref.dereference(channel)
                if component.artifacts:
                    flag = "({})".format(
                        ", ".join(sorted(component.artifacts.keys()))
                    )
                else:
                    raise KeyError(ref.package)

            except KeyError:
                flag = "[not available]"

            print("    {} {}".format(target, flag), file=stream)


def format_channel(channel, host_triple):
    result = io.StringIO()

    print("Date:", str(channel.date), file=result)

    rust = channel.packages["rust"]

    print("Version:", rust.version, file=result)

    if host_triple not in rust.targets:
        print("No components for triple", repr(host_triple), file=result)

    else:
        print("Host triple:", host_triple, file=result)
        major_component = rust.targets[host_triple]

        print("Required components:", file=result)
        format_refs(major_component.required_components, result, channel)

        print("Optional components:", file=result)
        format_refs(major_component.optional_components, result, channel)

    return result.getvalue()

