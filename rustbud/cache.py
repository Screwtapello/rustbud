import bz2
import gzip
import hashlib
import logging
import lzma
import os
import pathlib
import shutil
import stat
import tarfile
import tempfile

import attr
import boltons.fileutils
import requests
from xdg import BaseDirectory


LOGGER = logging.getLogger(__name__)


# Mask for "any executable bit" in a POSIX file mode.
S_IXUGO = stat.S_IXUSR | stat.S_IXGRP | stat.S_IXOTH


# Try to feed the hashing function with big chunks of data to minimize
# overhead.
HASH_CHUNK_SIZE = 8192


@attr.s(frozen=True)
class FileSet:

    base_path = attr.ib(
        validator=attr.validators.instance_of(pathlib.Path),
        cmp=True,
    )

    file_paths = attr.ib(
        validator=attr.validators.instance_of(frozenset),
        cmp=True,
    )


def safe_extract(tarfile, target):
    # Fetch the current process umask
    umask = os.umask(0)

    # Unfortunately, fetching it also sets it, so let's put it back.
    os.umask(umask)

    for member in tarfile:
        LOGGER.debug("Examining member %r", member)
        member_path = pathlib.PurePosixPath(member.name)

        if member_path.is_absolute():
            LOGGER.warn(
                "Ignoring absolute path %r",
                member_path,
            )
            continue

        if ".." in member_path.parts:
            LOGGER.warn(
                "Ignoring path with suspicious '..' segment: %r",
                member_path,
            )
            continue

        if not member.isfile():
            LOGGER.debug("Ignoring non-file path: %r", member_path)
            continue

        # Actually extract this member to the target path.
        target_path = target / member_path
        target_path.parent.mkdir(parents=True, exist_ok=True)
        shutil.copyfileobj(
            tarfile.extractfile(member),
            target_path.open(mode="wb"),
        )

        if member.mode & S_IXUGO:
            # Rather than respect the permissions in the tarball, if the
            # file was stored with any executable bit set, let's set all
            # of them (this is similar to how git works).
            # 
            # Of course, we should respect the umask too. Because
            # Python's open() is modelled on C's fopen(), we know that
            # the file will have been created with mode 0666 (modified
            # by umask), so we can safely assume the executable version
            # of that is 0777 modified by umask.
            target_path.chmod(0o777 & ~umask)


class Archive(dict):

    @classmethod
    def from_artifact(cls, handle, staging_path):
        try:
            staging_path.mkdir(parents=True)

        except FileExistsError:
            # This artifact has already been extracted.
            pass

        else:
            # We just created the staging path, let's populate it.
            safe_extract(tarfile.open(fileobj=handle), staging_path)

        return cls.from_extracted_artifact(staging_path)

    @classmethod
    def from_extracted_artifact(cls, staging_path):
        res = cls()

        for components_path in staging_path.glob("*/components"):
            LOGGER.debug("Found components file: %r", components_path)
            for component in components_path.open():
                component = component.rstrip("\n")
                LOGGER.debug("Found component: %r", component)
                component_files = set()
                manifest_path = (
                    components_path.parent / component / "manifest.in"
                )
                LOGGER.debug("Looking for manifest file: %r", manifest_path)
                for line in manifest_path.open():
                    line = line.rstrip("\n")
                    LOGGER.debug("Found manifest directive: %r", line)

                    kind, relpath = line.split(":", 1)
                    if kind == "file":
                        component_files.add(manifest_path.parent / relpath)
                    elif kind == "dir":
                        glob_path = manifest_path.parent / relpath
                        component_files.update(
                            each
                            for each in glob_path.glob("**/*")
                            if each.is_file()
                        )
                    else:
                        LOGGER.warn(
                            "Ignoring unknown kind %r in manifest %r line: %r",
                            kind,
                            manifest_path,
                            line,
                        )

                res[component] = FileSet(
                    manifest_path.parent,
                    frozenset(
                        each.relative_to(manifest_path.parent)
                        for each in component_files
                    ),
                )

        return res


class Cache:

    def __init__(self, path):
        self._path = path
        self._path.mkdir(parents=True, exist_ok=True)

    def validate(self, artifact_ref):
        item_path = self.item_path(artifact_ref)
        
        handle = item_path.open("rb")

        handle.seek(0, os.SEEK_END)
        total_size = handle.tell()
        handle.seek(0)

        hasher = hashlib.sha256()
        chunk = b'a'  # just some non-empty value
        while len(chunk) != 0:
            chunk = handle.read(HASH_CHUNK_SIZE)
            hasher.update(chunk)
            yield (len(chunk), total_size)

        if hasher.hexdigest() != artifact_ref.hash:
            LOGGER.warn(
                "File at %r has bad hash %r (expected %r), removing it",
                item_path,
                hasher.hexdigest(),
                artifact_ref.hash,
            )
            item_path.unlink()
            raise FileNotFoundError(str(item_path))

    def store(self, artifact_ref):
        item_path = self.item_path(artifact_ref)

        response = requests.get(artifact_ref.url, stream=True)
        response.raise_for_status()

        try:
            total_size = int(response.headers["content-length"])
        except (KeyError, ValueError):
            total_size = None

        with boltons.fileutils.AtomicSaver(str(item_path)) as handle:
            hasher = hashlib.sha256()
            for chunk in response.iter_content(HASH_CHUNK_SIZE):
                handle.write(chunk)
                hasher.update(chunk)
                yield (len(chunk), total_size)

        if hasher.hexdigest() == artifact_ref.hash:
            LOGGER.info(
                "Downloaded file at %r has expected hash %r",
                item_path,
                artifact_ref.hash,
            )
            return True
        
        else:
            LOGGER.warn(
                "Downloaded file at %r has bad hash %r (expected %r), removing it",
                item_path,
                hasher.hexdigest(),
                artifact_ref.hash,
            )
            item_path.unlink()
            return False

    def item_path(self, artifact_ref):
        return self._path / artifact_ref.hash

    def get(self, artifact_ref):
        item_path = self.item_path(artifact_ref)

        return Archive.from_artifact(
            item_path.open("rb"),
            item_path.with_suffix(".d"),
        )
