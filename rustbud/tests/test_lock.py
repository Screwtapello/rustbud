import datetime
import unittest

from rustbud import spec
from rustbud import channel
from rustbud import lock

def make_channel():
    return channel.Channel(
        date=datetime.date(2017, 5, 27),
        packages={
            "rust": channel.Package(
                version="1.2.3",
                targets={
                    "host-triple-1": channel.Component(
                        artifacts={
                            "gz": channel.ArtifactRef(
                                hash="rust-1-hash-gz",
                                url="http://example.com/rust-1.tar.gz",
                            ),
                            "xz": channel.ArtifactRef(
                                hash="rust-1-hash-xz",
                                url="http://example.com/rust-1.tar.xz",
                            ),
                        },
                        required_components=[
                            channel.ComponentRef(
                                package="cargo",
                                target="host-triple-1",
                            ),
                            channel.ComponentRef(
                                package="rustc",
                                target="host-triple-1",
                            ),
                            channel.ComponentRef(
                                package="rust-std",
                                target="host-triple-1",
                            ),
                        ],
                        optional_components=[
                             channel.ComponentRef(
                                 package="rust-src",
                                 target="*",
                             ),
                            channel.ComponentRef(
                                package="rust-std",
                                target="host-triple-2",
                            ),
                            channel.ComponentRef(
                                package="rust-std",
                                target="unavailable-target-3",
                            ),
                        ],
                    ),
                    "host-triple-2": channel.Component(
                        artifacts={
                            "gz": channel.ArtifactRef(
                                hash="rust-2-hash-gz",
                                url="http://example.com/rust-2.tar.gz",
                            ),
                            "xz": channel.ArtifactRef(
                                hash="rust-2-hash-xz",
                                url="http://example.com/rust-2.tar.xz",
                            ),
                        },
                        required_components=[
                            channel.ComponentRef(
                                package="cargo",
                                target="host-triple-2",
                            ),
                            channel.ComponentRef(
                                package="rustc",
                                target="host-triple-2",
                            ),
                            channel.ComponentRef(
                                package="rust-std",
                                target="host-triple-2",
                            ),
                        ],
                        optional_components=[
                             channel.ComponentRef(
                                 package="rust-src",
                                 target="*",
                             ),
                            channel.ComponentRef(
                                package="rust-std",
                                target="host-triple-1",
                            ),
                            channel.ComponentRef(
                                package="rust-std",
                                target="unavailable-target-3",
                            ),
                        ],
                    ),
                },
            ),
            "cargo": channel.Package(
                version="2.3.4",
                targets={
                    "host-triple-1": channel.Component(
                        artifacts={
                            "gz": channel.ArtifactRef(
                                hash="cargo-1-hash-gz",
                                url="http://example.com/cargo-1.tar.gz",
                            ),
                            "xz": channel.ArtifactRef(
                                hash="cargo-1-hash-xz",
                                url="http://example.com/cargo-1.tar.xz",
                            ),
                        },
                        required_components=[],
                        optional_components=[],
                    ),
                    "host-triple-2": channel.Component(
                        artifacts={
                            "gz": channel.ArtifactRef(
                                hash="cargo-2-hash-gz",
                                url="http://example.com/cargo-2.tar.gz",
                            ),
                            "xz": channel.ArtifactRef(
                                hash="cargo-2-hash-xz",
                                url="http://example.com/cargo-2.tar.xz",
                            ),
                        },
                        required_components=[],
                        optional_components=[],
                    ),
                },
            ),
            "rustc": channel.Package(
                version="3.4.5",
                targets={
                    "host-triple-1": channel.Component(
                        artifacts={
                            "gz": channel.ArtifactRef(
                                hash="rustc-1-hash-gz",
                                url="http://example.com/rustc-1.tar.gz",
                            ),
                            "xz": channel.ArtifactRef(
                                hash="rustc-1-hash-xz",
                                url="http://example.com/rustc-1.tar.xz",
                            ),
                        },
                        required_components=[],
                        optional_components=[],
                    ),
                    "host-triple-2": channel.Component(
                        artifacts={
                            "gz": channel.ArtifactRef(
                                hash="rustc-2-hash-gz",
                                url="http://example.com/rustc-2.tar.gz",
                            ),
                            "xz": channel.ArtifactRef(
                                hash="rustc-2-hash-xz",
                                url="http://example.com/rustc-2.tar.xz",
                            ),
                        },
                        required_components=[],
                        optional_components=[],
                    ),
                },
            ),
            "rust-std": channel.Package(
                version="3.4.5",
                targets={
                    "host-triple-1": channel.Component(
                        artifacts={
                            "gz": channel.ArtifactRef(
                                hash="rust-std-1-hash-gz",
                                url="http://example.com/rust-std-1.tar.gz",
                            ),
                            "xz": channel.ArtifactRef(
                                hash="rust-std-1-hash-xz",
                                url="http://example.com/rust-std-1.tar.xz",
                            ),
                        },
                        required_components=[],
                        optional_components=[],
                    ),
                    "host-triple-2": channel.Component(
                        artifacts={
                            "gz": channel.ArtifactRef(
                                hash="rust-std-2-hash-gz",
                                url="http://example.com/rust-std-2.tar.gz",
                            ),
                            "xz": channel.ArtifactRef(
                                hash="rust-std-2-hash-xz",
                                url="http://example.com/rust-std-2.tar.xz",
                            ),
                        },
                        required_components=[],
                        optional_components=[],
                    ),
                    "unavailable-target-3": channel.Component(
                        # This target did not build successfully at the
                        # time this channel manifest was produced.
                        artifacts={},
                        required_components=[],
                        optional_components=[],
                    ),
                },
            ),
            "rust-src": channel.Package(
                version="1.2.3",
                targets={
                    "*": channel.Component(
                        artifacts={
                            "gz": channel.ArtifactRef(
                                hash="rust-src-hash-gz",
                                url="http://example.com/rust-src.tar.gz",
                            ),
                            "xz": channel.ArtifactRef(
                                hash="rust-src-hash-xz",
                                url="http://example.com/rust-src.tar.xz",
                            ),
                        },
                        required_components=[],
                        optional_components=[],
                    ),
                },
            ),
        },
    )

class TestComponent(unittest.TestCase):

    def test_from_data(self):
        self.assertEqual(
            channel.ArtifactRef.from_data(
                {"url": "http://example.com/foo", "hash": "abc123"},
            ),
            channel.ArtifactRef(
                url="http://example.com/foo",
                hash="abc123",
            ),
        )

    def test_to_data(self):
        self.assertEqual(
            channel.ArtifactRef(
                url="http://example.com/foo",
                hash="abc123",
            ).to_data(),
            {"url": "http://example.com/foo", "hash": "abc123"},
        )

class TestLock(unittest.TestCase):

    def setUp(self):
        # Trick Python into comparing Lock values with
        # assertMultiLineEqual(), which I find easier to read.
        self.addTypeEqualityFunc(
            lock.Lock,
            lambda a, b, msg=None: self.assertEqual(
                repr(a).replace(", ", ",\n"),
                repr(b).replace(", ", ",\n"),
                msg,
            )
        )

    def test_from_data(self):
        self.assertEqual(
            lock.Lock.from_data(
                {
                    "toolchain": [
                        {"url": "http://example.com/foo", "hash": "abc123"},
                        {"url": "http://example.com/bar", "hash": "123abc"},
                    ],
                },
            ),
            lock.Lock([
                channel.ArtifactRef(
                    url="http://example.com/foo",
                    hash="abc123",
                ),
                channel.ArtifactRef(
                    url="http://example.com/bar",
                    hash="123abc",
                ),
            ]),
        )

    def test_to_data(self):
        self.assertEqual(
            lock.Lock([
                channel.ArtifactRef(
                    url="http://example.com/foo",
                    hash="abc123",
                ),
                channel.ArtifactRef(
                    url="http://example.com/bar",
                    hash="123abc",
                ),
            ]).to_data(),
            {
                "toolchain": [
                    {"url": "http://example.com/bar", "hash": "123abc"},
                    {"url": "http://example.com/foo", "hash": "abc123"},
                ],
            },
        )

    def test_from_empty_spec_uses_defaults_for_host_triple(self):
        envspec = spec.Spec()
        chan = make_channel()

        # By default, we expect all the required components for
        # "rust" for the given host triple, and none of the optional
        # ones.
        self.assertEqual(
            lock.Lock.from_spec(envspec, chan, "host-triple-1"),
            lock.Lock([
                channel.ArtifactRef(
                    url="http://example.com/cargo-1.tar.xz",
                    hash="cargo-1-hash-xz",
                ),
                channel.ArtifactRef(
                    url="http://example.com/rustc-1.tar.xz",
                    hash="rustc-1-hash-xz",
                ),
                channel.ArtifactRef(
                    url="http://example.com/rust-std-1.tar.xz",
                    hash="rust-std-1-hash-xz",
                ),
            ]),
        )

        # If we ask for a different host-triple, we get different
        # artifacts.
        self.assertEqual(
            lock.Lock.from_spec(envspec, chan, "host-triple-2"),
            lock.Lock([
                channel.ArtifactRef(
                    url="http://example.com/cargo-2.tar.xz",
                    hash="cargo-2-hash-xz",
                ),
                channel.ArtifactRef(
                    url="http://example.com/rustc-2.tar.xz",
                    hash="rustc-2-hash-xz",
                ),
                channel.ArtifactRef(
                    url="http://example.com/rust-std-2.tar.xz",
                    hash="rust-std-2-hash-xz",
                ),
            ]),
        )

    def test_present_required_component_is_locked(self):
        # Make sure we always have rust-std for host-triple-2.
        envspec = spec.Spec(
            required_components=frozenset([
                spec.Pattern("rust-std", "host-triple-2"),
            ]),
        )
        chan = make_channel()

        # Because we require rust-std for host-triple-2, it'll be present
        # even if we lock the environment for host-triple-1.
        self.assertEqual(
            lock.Lock.from_spec(envspec, chan, "host-triple-1"),
            lock.Lock([
                channel.ArtifactRef(
                    url="http://example.com/cargo-1.tar.xz",
                    hash="cargo-1-hash-xz",
                ),
                channel.ArtifactRef(
                    url="http://example.com/rustc-1.tar.xz",
                    hash="rustc-1-hash-xz",
                ),
                channel.ArtifactRef(
                    url="http://example.com/rust-std-1.tar.xz",
                    hash="rust-std-1-hash-xz",
                ),
                channel.ArtifactRef(
                    url="http://example.com/rust-std-2.tar.xz",
                    hash="rust-std-2-hash-xz",
                ),
            ]),
        )

        # If we're locking for host-tuple-2, of course, we'll have the
        # right version of rust-std anyway, and don't need to install
        # anything extra.
        self.assertEqual(
            lock.Lock.from_spec(envspec, chan, "host-triple-2"),
            lock.Lock([
                channel.ArtifactRef(
                    url="http://example.com/cargo-2.tar.xz",
                    hash="cargo-2-hash-xz",
                ),
                channel.ArtifactRef(
                    url="http://example.com/rustc-2.tar.xz",
                    hash="rustc-2-hash-xz",
                ),
                channel.ArtifactRef(
                    url="http://example.com/rust-std-2.tar.xz",
                    hash="rust-std-2-hash-xz",
                ),
            ]),
        )

    def test_reject_missing_required_target(self):
        # Require an invalid target for a valid package.
        envspec = spec.Spec(
            required_components=frozenset([
                spec.Pattern("cargo", "no-such-triple"),
            ]),
        )
        chan = make_channel()

        # Since the spec requires a version of cargo that does not
        # exist, we cannot lock it.
        with self.assertRaises(ValueError) as cm:
            lock.Lock.from_spec(envspec, chan, "host-triple-1")

        self.assertEqual(
            str(cm.exception),
            "Package 'cargo' has no available targets matching "
                "'no-such-triple'",
        )

    def test_reject_missing_required_package(self):
        # Require a valid target for an invalid package.
        envspec = spec.Spec(
            required_components=frozenset([
                spec.Pattern("bogus", "host-triple-1"),
            ]),
        )
        chan = make_channel()

        # Since the spec requires a package that does not exist,
        # we cannot lock it.
        with self.assertRaises(ValueError) as cm:
            lock.Lock.from_spec(envspec, chan, "host-triple-1")

        self.assertEqual(
            str(cm.exception),
            "Package 'bogus' has no available targets matching "
                "'host-triple-1'",
        )

    def test_reject_unavailable_required_package(self):
        # Require a valid-but-unavailable target for a valid package.
        envspec = spec.Spec(
            required_components=frozenset([
                spec.Pattern("rust-std", "unavailable-target-3"),
            ]),
        )
        chan = make_channel()

        # Since this target for this package is not available,
        # we cannot lock the spec..
        with self.assertRaises(ValueError) as cm:
            lock.Lock.from_spec(envspec, chan, "host-triple-1")

        self.assertEqual(
            str(cm.exception),
            "Package 'rust-std' target 'unavailable-target-3' is not "
                "available in any acceptable format ('xz', 'gz')",
        )

    def test_present_optional_component_is_locked(self):
        # Request an available component marked as optional.
        envspec = spec.Spec(
            optional_components=frozenset([
                spec.Pattern("rust-std", "host-triple-2"),
            ]),
        )
        chan = make_channel()

        # Because we request rust-std for host-triple-2, it'll be present
        # even if we lock the environment for host-triple-1.
        self.assertEqual(
            lock.Lock.from_spec(envspec, chan, "host-triple-1"),
            lock.Lock([
                channel.ArtifactRef(
                    url="http://example.com/cargo-1.tar.xz",
                    hash="cargo-1-hash-xz",
                ),
                channel.ArtifactRef(
                    url="http://example.com/rustc-1.tar.xz",
                    hash="rustc-1-hash-xz",
                ),
                channel.ArtifactRef(
                    url="http://example.com/rust-std-1.tar.xz",
                    hash="rust-std-1-hash-xz",
                ),
                channel.ArtifactRef(
                    url="http://example.com/rust-std-2.tar.xz",
                    hash="rust-std-2-hash-xz",
                ),
            ]),
        )

        # If we're locking for host-tuple-2, of course, we'll have the
        # right version of rust-std anyway, and don't need to install
        # anything extra.
        self.assertEqual(
            lock.Lock.from_spec(envspec, chan, "host-triple-2"),
            lock.Lock([
                channel.ArtifactRef(
                    url="http://example.com/cargo-2.tar.xz",
                    hash="cargo-2-hash-xz",
                ),
                channel.ArtifactRef(
                    url="http://example.com/rustc-2.tar.xz",
                    hash="rustc-2-hash-xz",
                ),
                channel.ArtifactRef(
                    url="http://example.com/rust-std-2.tar.xz",
                    hash="rust-std-2-hash-xz",
                ),
            ]),
        )

    def test_allow_missing_optional_target(self):
        # Request an unknown target for a valid package.
        envspec = spec.Spec(
            optional_components=frozenset([
                spec.Pattern("rust-std", "no-such-triple"),
            ]),
        )
        chan = make_channel()

        # That's fine, we just won't lock it.
        self.assertEqual(
            lock.Lock.from_spec(envspec, chan, "host-triple-1"),
            lock.Lock([
                channel.ArtifactRef(
                    url="http://example.com/cargo-1.tar.xz",
                    hash="cargo-1-hash-xz",
                ),
                channel.ArtifactRef(
                    url="http://example.com/rustc-1.tar.xz",
                    hash="rustc-1-hash-xz",
                ),
                channel.ArtifactRef(
                    url="http://example.com/rust-std-1.tar.xz",
                    hash="rust-std-1-hash-xz",
                ),
            ]),
        )

    def test_allow_missing_optional_package(self):
        # Request a valid target for an unknown package.
        envspec = spec.Spec(
            optional_components=frozenset([
                spec.Pattern("bogus", "host-triple-1"),
            ]),
        )
        chan = make_channel()

        # That's fine, we just won't lock it.
        self.assertEqual(
            lock.Lock.from_spec(envspec, chan, "host-triple-1"),
            lock.Lock([
                channel.ArtifactRef(
                    url="http://example.com/cargo-1.tar.xz",
                    hash="cargo-1-hash-xz",
                ),
                channel.ArtifactRef(
                    url="http://example.com/rustc-1.tar.xz",
                    hash="rustc-1-hash-xz",
                ),
                channel.ArtifactRef(
                    url="http://example.com/rust-std-1.tar.xz",
                    hash="rust-std-1-hash-xz",
                ),
            ]),
        )

    def test_allow_unavailable_optional_target(self):
        # Request a valid-but-unavailable target for a valid package.
        envspec = spec.Spec(
            optional_components=frozenset([
                spec.Pattern("rust-std", "unavailable-target-3"),
            ]),
        )
        chan = make_channel()

        # That's fine, we just won't lock it.
        self.assertEqual(
            lock.Lock.from_spec(envspec, chan, "host-triple-1"),
            lock.Lock([
                channel.ArtifactRef(
                    url="http://example.com/cargo-1.tar.xz",
                    hash="cargo-1-hash-xz",
                ),
                channel.ArtifactRef(
                    url="http://example.com/rustc-1.tar.xz",
                    hash="rustc-1-hash-xz",
                ),
                channel.ArtifactRef(
                    url="http://example.com/rust-std-1.tar.xz",
                    hash="rust-std-1-hash-xz",
                ),
            ]),
        )

    def test_select_artifacts_in_priority_order(self):
        # Request some components, but require gz format.
        envspec = spec.Spec(
            formats=("gz",),
            required_components=frozenset([
                spec.Pattern("rust-src", "*"),
            ]),
            optional_components=frozenset([
                spec.Pattern("rust-std", "host-triple-2"),
            ]),
        )
        chan = make_channel()

        # All our artifacts should be .tar.gz files.
        self.assertEqual(
            sorted(
                lock.Lock.from_spec(envspec, chan, "host-triple-1"),
            ),
            [
                channel.ArtifactRef(
                    url="http://example.com/cargo-1.tar.gz",
                    hash="cargo-1-hash-gz",
                ),
                channel.ArtifactRef(
                    url="http://example.com/rust-src.tar.gz",
                    hash="rust-src-hash-gz",
                ),
                channel.ArtifactRef(
                    url="http://example.com/rust-std-1.tar.gz",
                    hash="rust-std-1-hash-gz",
                ),
                channel.ArtifactRef(
                    url="http://example.com/rust-std-2.tar.gz",
                    hash="rust-std-2-hash-gz",
                ),
                channel.ArtifactRef(
                    url="http://example.com/rustc-1.tar.gz",
                    hash="rustc-1-hash-gz",
                ),
            ],
        )
