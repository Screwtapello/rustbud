import argparse
import io
import logging
import os
import os.path
import pathlib
import pwd

import requests
import toml
from xdg import BaseDirectory

import rustbud
import rustbud.cache
import rustbud.channel


LOGGER = logging.getLogger(__name__)


def guess_host_triple(uname):
    platform = (uname.sysname, uname.machine)

    if platform == ("Linux", "x86_64"):
        return "x86_64-unknown-linux-gnu"

    raise ValueError("Unknown platform: {!r}".format(platform))


def guess_shell(environ, pwent):
    alternatives = [
        environ.get("SHELL", ""),
        pwent.pw_shell,
        "/bin/sh",
    ]
    for each in alternatives:
        if each:
            return each


def guess_artifact_names(spec_path):
    LOGGER.debug("Got name with suffix: %r", spec_path.suffix)

    spec_name = spec_path.name
    spec_suffix = "-spec.toml"

    if spec_name.lower().endswith(spec_suffix):
        basename = spec_name[:-len(spec_suffix)]
    else:
        basename = spec_name

    return (
        spec_path.with_name(basename + "-lock.json"),
        spec_path.with_name(basename + "-env"),
    )


def open_channel_url(maybe_url, relative_to):
    # Expand our channel keywords.
    maybe_url = rustbud.CHANNEL_METADATA_URLS.get(maybe_url, maybe_url)

    # Let's check for a local file first, because that's easiest.
    try:
        return relative_to.joinpath(maybe_url).open()
    except IOError:
        LOGGER.warn("Channel %r is not a file path, trying as a URL", maybe_url)
        pass

    # Maybe it really is a URL.
    response = requests.get(maybe_url)
    response.raise_for_status()
    return io.StringIO(response.text)


def make_artifact_cache():
    return rustbud.cache.Cache(
        pathlib.Path(BaseDirectory.save_cache_path("rustbud", "downloads"))
    )


def list_channel():
    logging.basicConfig()

    uname = os.uname()
    default_host_triple = guess_host_triple(uname)

    parser = argparse.ArgumentParser(
        description="List the components in a channel.",
    )
    parser.add_argument(
        "-t", "--host-triple",
        metavar="TRIPLE",
        default=default_host_triple,
        help="List components that can be used on the platform described "
            "by TRIPLE. This setting is unrelated to the target triple, "
            "which describes where programs compiled by Rust will execute. "
            "[default: %(default)s]",
    )
    parser.add_argument(
        "-c", "--channel",
        metavar="URL",
        default="stable",
        help="List the components in the channel manifest found at URL. "
            "URL can be an absolute URL, an absolute or relative filesystem "
            "path, or one of the special keywords 'stable', 'beta' or "
            "'nightly' to use the official Rust release channels. "
            "[default: %(default)s]",
    )

    options = parser.parse_args()

    with open_channel_url(options.channel, pathlib.Path.cwd()) as handle:
        manifest = toml.load(handle)

    channel = rustbud.channel.Channel.from_manifest(manifest)

    print(
        rustbud.channel.format_channel(
            channel,
            options.host_triple,
        )
    )

    return os.EX_OK


def list_artifact():
    logging.basicConfig()

    parser = argparse.ArgumentParser(
        description="List the contents of a Rust toolchain artifact.",
    )
    parser.add_argument(
        "url",
        metavar="URL",
        help="The URL of the toolchain artifact to inspect.",
    )
    parser.add_argument(
        "hash",
        metavar="HASH",
        help="The SHA256 of the toolchain artifact, as lower-case hex digits.",
    )

    options = parser.parse_args()

    ref = rustbud.channel.ArtifactRef(options.hash, options.url)
    cache = make_artifact_cache()
    cache_artifact(cache, ref)

    archive = cache.get(ref)

    for component in sorted(archive):
        print(component)
        for path in sorted(archive[component].file_paths):
            print("  ", path)

    return os.EX_OK


def env_file_argument(raw):
    env_file = pathlib.Path(os.path.abspath(raw))

    if env_file.exists() and not env_file.is_file():
        raise argparse.ArgumentTypeError(
            "environment spec {} is not a file!".format(raw),
        )

    return env_file


def env_lock_argument(raw):
    env_lock = pathlib.Path(os.path.abspath(raw))

    if env_lock.exists() and not env_lock.is_file():
        raise argparse.ArgumentTypeError(
            "environment lock {} is not a file!".format(raw),
        )

    return env_lock


def env_path_argument(raw):
    env_path = pathlib.Path(os.path.abspath(raw))

    if env_path.exists() and not env_path.is_dir():
        raise argparse.ArgumentTypeError(
            "environment path {} is not a directory!".format(raw),
        )

    return env_path


def activate():
    logging.basicConfig()

    shell = guess_shell(
        os.environ,
        pwd.getpwuid(os.getuid()),
    )

    parser = argparse.ArgumentParser(
        description="Activate a previously-created environment.",
    )
    parser.add_argument(
        "-t", "--host-triple",
        metavar="TRIPLE",
        default=guess_host_triple(os.uname()),
        help="Build the environment from components intended to run on the "
            "platform described by TRIPLE. This setting is unrelated to the "
            "target triple, which describes where programs compiled by Rust "
            "will execute. [default: %(default)s]",
    )
    parser.add_argument(
        "-f", "--env-file",
        metavar="FILE",
        type=env_file_argument,
        default="rustbud-spec.toml",
        help="Read the desired environment description from FILE. "
            "[default: %(default)s]",
    )
    parser.add_argument(
        "-l", "--lock-file",
        metavar="FILE",
        type=env_lock_argument,
        help="Write the calculated environment details to FILE. "
            "[default: based on env-file]",
    )
    parser.add_argument(
        "-p", "--env-path",
        metavar="DIR",
        type=env_path_argument,
        help="Install the toolchain artifacts into DIR. "
            "[default: based on env-file]",
    )

    parser.add_argument(
        "prog",
        metavar="COMMAND",
        nargs="?",
        default=shell,
        help="Run COMMAND inside the activated environment. "
            "Any following arguments will be passed through to COMMAND. "
            "[default: %(default)s]",
    )
    parser.add_argument(
        "progargs",
        nargs=argparse.REMAINDER,
        help=argparse.SUPPRESS,
    )

    options = parser.parse_args()

    default_env_lock, default_env_path = guess_artifact_names(options.env_file)

    env = rustbud.Environment(
        options.host_triple,
        options.env_file,
        options.lock_file or default_env_lock,
        options.env_path or default_env_path,
    )

    env.activate([options.prog] + options.progargs)
