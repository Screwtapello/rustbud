Rustbud, an alternative Rust toolchain manager
==============================================

Rustbud is a tool to download and set up a Rust build environment
(including `rustc`, `cargo`, documentation, and various other tools).

Rustbud uses the official binaries hosted by the Rust project.

Compared to other toolchain managers
====================================

If Rustbud is an alternative toolchain manager, how does it compare?

Versus rustup
-------------

[rustup](https://www.rustup.rs/) is the Rust project's official toolchain
manager.

<table>
    <thead>
        <th width="50%">rustbud</th>
        <th width="50%">rustup</th>
    </thead>
    <tbody>
        <tr>
            <td>
                Rustbud expects you to create a separate environment
                for each project, preventing changes made for one project
                from breaking the others.
            </td>
            <td>
                rustup expects you to share the same few environments
                across all your projects.
            </td>
        </tr>
        <tr>
            <td>
                Rustbud generates environments from a single, declarative
                file that can be checked into source-control, allowing
                contributors (or automated build systems) to quickly
                set up a suitable environment.
            </td>
            <td>
                rustup environments are normally created interactively
                and can be modified at any time.
            </td>
        </tr>
        <tr>
            <td>
                A Rustbud environment can be exactly recreated from the
                <code>rustbud-lock.json</code> file (much like
                <code>Cargo.lock</code>), allowing you to reconstruct
                the specific toolchain used to build historical versions
                of your software.
            </td>
            <td>
                rustup cannot serialise the state of an environment.
            </td>
        </tr>
        <tr>
            <td>
                Rustbud stores environment state in
                the current directory (by default) while
                downloads and other disposable data goes in the <a
                href="https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html">standard
                XDG cache directory</a>, so it's easy to free up disk space
                without losing anything valuable.
            </td>
            <td>
                rustup mixes discardable caches and environment state
                in the <code>$RUSTUP_HOME</code> directory.
            </td>
        </tr>
        <tr>
            <td>
                Activating an environment launches a new shell with a custom
                <code>$PATH</code>, so processes outside aren't affected.
            </td>
            <td>
                rustup puts "wrapper binaries" on your default
                <code>$PATH</code> that decide which environment to run inside.
            </td>
        </tr>
        <tr>
            <td>
                Rustbud is very new, and is made by some guy on the Internet.
            </td>
            <td>
                rustup has been around for a long time,
                and is officially supported by the Rust project.
            </td>
        </tr>
        <tr>
            <td>
                Rustbud follows Unix platform conventions, and is designed
                to take advantage of Unix features where possible.
            </td>
            <td>
                rustup is designed for maximum portability, and to work
                the same way everywhere.
            </td>
        </tr>
        <tr>
            <td>
                Rustbud is designed to be composable, repeatable and
                predictable, and work well as part of a larger system.
            </td>
            <td>
                rustup is designed to be convenient and malleable,
                and work well for humans to use directly.
            </td>
        </tr>
   </tbody>
</table>

Versus your existing package manager
------------------------------------

Many system package managers (including [Debian][debrust] and
[Fedora][fedrust]) and third-party package managers (like [Nix][nixrust] and
[Homebrew][brwrust]) already package a Rust toolchain.

[debrust]: https://tracker.debian.org/pkg/rustc
[fedrust]: https://apps.fedoraproject.org/packages/rust
[brwrust]: http://brewformulas.org/rust
[nixrust]: https://github.com/NixOS/nixpkgs/tree/master/pkgs/development/compilers/rust

<table>
    <thead>
        <th width="50%">rustbud</th>
        <th width="50%">existing package manager</th>
    </thead>
    <tbody>
        <tr>
            <td>
                Rustbud installs up-to-date binaries built by the Rust project.
            </td>
            <td>
                Your existing package manager probably provides whatever
                version of Rust was current when it was released, which
                may be months or years ago.
            </td>
        </tr>
        <tr>
            <td>
                Rustbud supports any number of isolated environments,
                with potentially different versions inside each one.
            </td>
            <td>
                Your existing package manager probably only provides one
                version of the Rust toolchain, installed system-wide.
            </td>
        </tr>
        <tr>
            <td>
                Rustbud installs generic binaries designed to work on
                a range of systems.
            </td>
            <td>
                Your existing package manager's binaries are probably
                customised to take advantage of everything your system
                provides.
            </td>
        </tr>
        <tr>
            <td>
                You are responsible for tracking what's installed in each
                environment, and applying security updates and bug-fixes
                as necessary.
            </td>
            <td>
                Rust toolchain fixes will be applied automatically just
                like everything else in your system.
            </td>
        </tr>
   </tbody>
</table>

Installation
============

The current experimental version of Rustbud is written in Python.
Hopefully that will change soon, but in the mean time...

Requirements
------------

 - Python 3.5 or above
 - `~/.local/bin` should be on your `$PATH`

Installing with Pip
-------------------

All modern Python installs should come with [Pip][pip], the Python
packaging tool. The easiest way to get Rustbud installed is:

    pip3 install --user https://gitlab.com/Screwtapello/rustbud/repository/master/archive.zip

[pip]: https://pip.pypa.io/en/stable/

To uninstall Rustbud with Pip:

    pip3 uninstall rustbud

Note that this won't uninstall any of Rustbud's dependencies, and requires
that you use the same version of Python that you installed with.

Installing with Pipsi
---------------------

[Pipsi][pipsi] is a wrapper around Pip specifically for installing
command-line tools like Rustbud, which keeps them and their dependencies
isolated from each other and from any other Python libraries or tools
you might have installed. I recommend installing Rustbud with Pipsi.

    # gotta bootstrap somehow :/
    curl https://raw.githubusercontent.com/mitsuhiko/pipsi/master/get-pipsi.py | python3

    pipsi install https://gitlab.com/Screwtapello/rustbud/repository/master/archive.zip#egg=rustbud

To uninstall Rustbud with Pipsi:

    pipsi uninstall rustbud

[pipsi]: https://github.com/mitsuhiko/pipsi

Using Rustbud
=============

To try out Rustbud for the first time, make a temporary directory and run
`rustbud-activate`:

    $ mkdir ~/rustbud-demo
    $ cd ~/rustbud-demo
    $ rustbud-activate

You'll see Rustbud print a lot of messages about downloading and
installing things, and then drop you back to the shell prompt. However,
this is not the original shell you started from, this is a subshell
configured to use the Rust toolchain you just downloaded:

    $ which rustc
    ~/rustbud-demo/rustbud-env/bin/rustc

(it's probably going to print the actual path to your home directory,
rather than abbreviating it to `~`)

From this shell, you can use `rustc` and `cargo` as normal. If you run
`cargo install`, the program will be installed into this custom
environment, isolated from other environments and your normal system
configuration.

    $ cargo install ripgrep
    $ which rg
    ~/rustbud-demo/rustbud-env/bin/rg

Once you're done using this environment, you can go back to your original
shell by exiting the subshell:

    $ exit

If you later need to activate this environment again, just run
`rustbud-activate` in the same directory. Since the environment was
already set-up, it doesn't need to download or install things again.

If you want to run a particular command inside the environment,
rather than an interactive shell, you can put it at the end of the
`rustbud-activate` command line:

    $ rustbud-activate cargo --version
    cargo 0.21.0 (5b4b8b2ae 2017-08-12)

Rustbud activates the environment for the duration of the command. The
command's exit-code is passed through unmodified, so you can use the
`rustbud-activate`-wrapped command pretty much anywhere you'd have used
the original command.

Choosing a host triple
----------------------

When Rustbud chooses a toolchain to download, it tries to pick one that
will run on your particular computer and operating system. Rust uses a
special name called (for obscure reasons) a "triple" to distinguish
the available variants. Rustbud has some very limited logic for guessing
which triple to use, but if it guesses incorrectly (or you deliberately
want a different version for some reason) you can use the `--host-triple`
option when you first create the environment:

    rustbud-activate --host-triple i686-unknown-linux-gnu

Some common triples are:

  - **x86_64-unknown-linux-gnu** for most Linux environments
  - **i686-unknown-linux-gnu** for old, 32-bit Linux environments
  - **x86_64-apple-darwin** for most macOS environments
  - **i686-apple-darwin** for old, 32-bit macOS environments

Note that "host triple" describes the environment where the compiler
runs. "target triple" describes the environment where the *output*
of the compiler runs.

Specs, Locks and Envs
---------------------

Conceptually, Rustbud works like this:

  - Rustbud reads the `rustbud-spec.toml` file in the current directory,
    which specifies the Rust release channel you want to use (stable,
    nightly, etc.) and what components from that channel to install
    (rustc, cargo, docs, etc.).
  - Rustbud downloads the manifest for the requested channel and figures out
    which downloads will provide the requested components.
  - Rustbud writes the chosen list of downloads to `rustbud-lock.json`
    in the same directory as `rustbud-spec.toml`.
  - Rustbud downloads all the files listed in `rustbud-lock.json` and
    installs them into a new directory named `rustbud-env`.

`rustbud-activate` will create `rustbud-env` if it doesn't already exist,
based on `rustbud-lock.json`. If that doesn't exist either, it will
be created based on `rustbud-spec.toml`. If that also doesn't exist,
it will use sensible defaults (stable channel, and all the components
that the stable channel marks "required").

Therefore, if you have a project with no special requirements, you can
use Rustbud with no extra ceremony. If you have a project that requires
Rust nightlies, you can create a `rustbud-spec.toml` that specifies the
Nightly channel. Much like `Cargo.lock`, you may choose to commit your
`rustbud-lock.json` to source-control, or ignore it, depending on how
much you care about reproducing the exact build environment for historical
versions of your software.

All these names can be overridden at activation-time, but the default
names for the lock file and env directory are derived from the spec
name so that you can have multiple specs in the same directory without
their locks or envs conflicting. For example, if you want to have a
separate build-environment for development and for deployment, you might
create `devel-spec.toml` and `deploy-spec.toml`. Activating
`devel-spec.toml` will create `devel-lock.json` and `devel-env`,
while activating `deploy-spec.toml` will create `deploy-lock.json` and
`deploy-env`.

Reference
=========

Spec files
----------

A Rustbud spec file uses [TOML][toml] syntax, and supports the following
configuration options:

  - `toolchain.channel` (string) specifies which Rust release channel to
    use. Default is `stable`. It may be:
      - one of the strings `stable`, `beta` or `nightly`, meaning the
        official Rust release channels.
      - An absolute or relative filesystem path to a channel manifest file.
        Relative paths are interpreted relative to the path of the spec file.
      - A URL with the `http` or `https` schemes.
  - `toolchain.formats` (list of strings) is an ordered list of archive
    formats. When a component is available in multiple formats, Rustbud will
    choose the one whose format appears earliest in this list. Default:
    `["xz", "gz"]`.
  - `toolchain.required.NAME` (list of strings) tells Rustbud to also
    install the component `NAME` for all the listed triples. For some
    components (like "rls") the relevant triple depends on the host where
    you're running Rustbud, and for some components (like "rust-src")
    triples aren't relevant at all, so you can use shell glob patterns
    like "*" to install all variants of a component whose triples match
    the pattern. If the component `NAME` is not listed in the channel
    manifest, or the listed targets are not available for it, Rustbud
    will fail to build the lock file.
  - `toolchain.optional.NAME` (list of strings) is like
    `toolchain.required.NAME` except that unknown or unavailable components
    are ignored.

Example:

```toml
[toolchain]
channel = "nightly"

[toolchain.required]
# We want to be able to build static Linux executables
rust-std=["x86_64-unknown-linux-musl"]

[toolchain.optional]
# We want to use the Rust Language Server if it's available.
rls=["*"]
```

[toml]: https://github.com/toml-lang/toml

TODO
====

Some of the things I want to add to Rustbud in the future:

  - Rewrite It In Rust™
  - lock files should record the format of each artefact (gz/xz) as declared
    in the channel file, so Rustbud doesn't have to guess.
  - lock files should record the URLs and hashes of all the variants
    for each chosen component, instead of only the current host triple.
  - lock files should not assume a 1:1 correspondence of artefacts
    and components.
      - maybe even decide whether to download the omnibus archive or
        individual components, based on whichever is smaller.
  - support a list of things to "cargo install" as well as the toolchain
      - this will require implementing cargo's version comparison syntax,
        and applying it to the list in
        `https://crates.io/api/v1/crates/$PKG/versions` so we can record
        a specific version in our lock file.
  - locally cache channel manifests, not just downloaded archives
  - support ignoring normally-required components in case you really don't
    need them for whatever reason (perhaps you don't need to download
    the documentation on a build-farm).
  - Mention where we're caching downloaded components on stderr
  - a standard config file to define the URLs used for the "stable",
    "beta" and "nightly" keywords
  - config options to specify how the lock-file and env-dir names are
    built from the spec-file name.
      - this will likely require some kind of runtime templating system,
        which is potentially a lot of complexity for a small feature.
  - I think RLS needs an environment variable telling it where to find
    the Rust source code; we should set that by default, whatever it is.
  - support custom envvars in `rustbud-spec.toml`.
    These will need to be saved to a file in the env, copied to the lock
    and probably to a file in the env, too.
      - This will be super useful for
        [sccache](https://github.com/mozilla/sccache/blob/master/docs/Rust.md)
  - Automatically rebuild the env if it's older than the lock file
  - Automatically rebuild the lock file if it's older than the spec file.
  - when creating the env, as the last step create some flag file inside
    it, which we can use to detect partially-built envs and also we can
    use its mtime for staleness-checking.
  - An option to error-out if `rustbud-activate` finds `$RUSTBUD_ROOT`
    in the environment (i.e. activating one env inside another)
  - Document how to display the currently-active environment in your
    bash prompt. Also, load the bash completion config that gets dumped
    into the `etc/` directory in the environment.
  - GPG-validate downloaded artefacts, don't just rely on validating
    their unsigned hashes.
  - Add more uname-to-triple smarts, at least for 64-bit and 32-bit
    macOS and Linux.
  - Investigate the viability of a Windows port. We can't use hard-links,
    so it'll be slower, and "set up environment variables" might not even
    be useful under Windows, but we should at least have a go.
  - Should environments be created in `~/.cache/` rather than beside
    the spec and lock? That would make it more likely that we could
    use hard-links, and keep some cruft out of working directories, but
    generating a unique, meaningful name for every possible environment
    is Difficult.
      - at least with the current scheme, it's obvious where the
        environment lives, and `git clean -dxf` will clean it up along
        with everything else.
  - Export an environment variable `$RUSTBUD_SPEC` that points to the
    spec file whose contents specified the current environment (even if
    the actual file doesn't exist). Since you can have multiple spec files
    in the same directory, `$RUSTBUD_ROOT` is not enough.
  - A tool that reads a lock file and produces a Dockerfile that will
    build a Docker image with the specified components pre-installed.
